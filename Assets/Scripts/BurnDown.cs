﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BurnDown : MonoBehaviour {

    private float lightAmount;
    public float val;
    public float particleFactor;

    public GameObject currentFire;

    private Light lightSource;
    private ParticleSystem particleSource;



    // Use this for initialization
    void Start()
    {
        lightSource = transform.FindChild("Point light").light;
        particleSource = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        lightSource.intensity -= Time.deltaTime;
        particleSource.emissionRate -= (Time.deltaTime * particleFactor);
        particleSource.startSize -= (Time.deltaTime / 2);
    }
}
