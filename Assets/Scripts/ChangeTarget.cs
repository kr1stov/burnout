﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[ExecuteInEditMode]

public class ChangeTarget : MonoBehaviour {

    private GameObject[] targets;
    
    private List<GameObject> targetsList;
    private List<float> targetXPositions;

    private ArrayList tal;
    
    public Transform target;
    private int currentTargetIndex;

    public float lerpTime;



    // Use this for initialization
	void Start () {
        tal = new ArrayList();

        targets = GameObject.FindGameObjectsWithTag("CampFire");
        currentTargetIndex = 0;

	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            currentTargetIndex = ClampIndex(++currentTargetIndex);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            currentTargetIndex = ClampIndex(--currentTargetIndex);
        }

        //GetComponent<SmoothFollow>().target = targets[currentTargetIndex].transform;
        target = targets[currentTargetIndex].transform;
        Vector3 targetPosition = new Vector3(target.position.x, transform.position.y, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, targetPosition, lerpTime);
        //Camera.main.GetComponent<ControlFire>().currentFire = target.gameObject;
	}

    int ClampIndex(int i)
    {
        if (i >= targets.Length)
            i = 0;
        else if (i < 0)
            i = targets.Length - 1;

        return i;
    }
}
