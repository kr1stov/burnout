﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class ControlFire : MonoBehaviour {

    private float lightAmount;
    public float val;

    private float totalVal;
    public float particleFactor;

    public GameObject currentFire;
    
    private Light lightSource;
    private ParticleSystem particleSource;

   

    // Use this for initialization
	void Start () {
        currentFire = Camera.main.GetComponent<ChangeTarget>().target.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            totalVal += val;

            totalVal = Mathf.Clamp(totalVal, 0, 8);
            ManipulateFire(totalVal);
        }
	}

    void ManipulateFire(float v)
    {
        currentFire = Camera.main.GetComponent<ChangeTarget>().target.gameObject;
        currentFire.transform.FindChild("Point light").light.intensity = v;
        currentFire.GetComponent<ParticleSystem>().emissionRate = v * particleFactor;
        currentFire.GetComponent<ParticleSystem>().startSize = v / 5;
    }
}
